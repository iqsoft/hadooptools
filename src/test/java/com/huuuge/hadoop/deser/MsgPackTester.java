package com.huuuge.hadoop.deser;

import com.google.common.base.Strings;
import com.google.common.primitives.Bytes;
import com.huuuge.hadoop.db.MetastoreService;
import com.huuuge.hadoop.db.dto.Metadata;
import com.huuuge.hadoop.db.event.EventLog;
import com.huuuge.hadoop.db.event.EventLogParser;
import com.huuuge.hadoop.db.event.EventTypeMapper;
import com.huuuge.hadoop.deser.dto.*;
import com.huuuge.hadoop.deser.dto.Data;
import org.apache.commons.codec.binary.Hex;
import org.apache.hadoop.io.MD5Hash;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by RWO on 6/15/2016.
 */
public class MsgPackTester {
    private static final Logger LOG = LoggerFactory.getLogger(MsgPackTester.class);

    public void produceBinaryDataFiles() throws IOException {
        String fileName = "20160523_000000_573db39f365998c8040b";
        File inputFile = new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".log");
        File outputFile = new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".bin");
//        List<Data> dataList = DataBuilder.buildDataFromEventsLogFile(new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".log"));
        BinaryDataFileProducer.generateFile(inputFile, outputFile);
    }

    public void consumeBinaryDataFiles() throws IOException {
        String fileName = "20160523_000000_573db39f365998c8040b";
        File inputFile = new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".bin");
        File outputFile = new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".log");
        BinaryDataFileConsumer.loadFile(inputFile, outputFile);
    }

    @Test
    public void checkDeser() {
        String fileName = "20160523_000000_573db39f365998c8040b";
        File inputFile = new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".log");
        File serFile = new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".ser");
        File desFile = new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".des");

        try {
            int serializedDataBlocks = BinaryDataFileProducer.generateFile(inputFile, serFile);
//            int deserializedDataBlocks = BinaryDataFileConsumer.loadFile(serFile, desFile);
            assertTrue(serializedDataBlocks == serializedDataBlocks);
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    public void checkDeser2() throws IOException {
        List<Data> serDataList = new ArrayList<>();
        MsgPackDataWriter dataWriter = new MsgPackDataWriter();
        MsgPackDataReader dataReader = new MsgPackDataReader();

        List<Data> desDataList = DataBuilder.buildDataFromEventsLogFile(new File("d:\\Huuuge\\Temp\\huuugeevents\\20160523_000000_573da94c36a53e23040b_part.log"));
        desDataList.forEach(desData -> {
            try {
                byte[] bytes = dataWriter.write(desData);
                LOG.info("Bytes size {} vs Json size {}", bytes.length, desData.toString().length());
                Data serData = dataReader.read(bytes);
                serDataList.add(serData);
            } catch (IOException e) {
                LOG.error("Exception during data serialization {}", desData.getHeader());
            }
        });
        assertTrue(serDataList.size() == (desDataList.size()));
    }

    private Data buildDataBlock() throws IOException {
        Header header = new Header("hc", "poker", "573d71ed34063a66042900000039", "18507");

        ColumnMetadata columnMetadata = new ColumnMetadata();
        columnMetadata.addColumn((short) 0, new Column("session_id"));
        columnMetadata.addColumn((short) 1, new Column("player_id"));
        columnMetadata.addColumn((short) 2, new Column("event_num"));
        columnMetadata.addColumn((short) 3, new Column("timestamp"));

        List<Event> events = new ArrayList<>();
        EventRow values = new EventRow();
        values.addValue((short) 0, Value.createValue((byte) 1, "573d71ed34063a66042900000039"));
        values.addValue((short) 1, Value.createValue((byte) 1, "18507"));
        values.addValue((short) 2, Value.createValue((byte) 5, 4));
        values.addValue((short) 3, Value.createValue((byte) 6, 1463947255341l));
        events.add(new Event(0, 1463947255341l, values));
        values.clearValues();
        values.addValue((short) 0, Value.createValue((byte) 1, "573d71ed34063a66042900000039"));
        values.addValue((short) 1, Value.createValue((byte) 1, "18507"));
        values.addValue((short) 2, Value.createValue((byte) 4, 5));
        values.addValue((short) 3, Value.createValue((byte) 6, 1463947259729l));
        events.add(new Event(1, 1463947259729l, values));

        Data data = new Data();
        data.setHeader(header);
        data.setColumnsMetadata(columnMetadata);
        data.setEvents(events);

        return data;
    }

    private static class DataBuilder {

        public static List<Data> buildDataFromEventsLogFile(File inputFile) {
            List<Data> dataList = new ArrayList<>();
            MetastoreService metastoreService = new MetastoreService();
            List<Metadata> columns = metastoreService.getTableMetadata("hcevents");

            ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);

            Header header = new Header();
            header.setProject("uhn");
            header.setSku("zeus");

            ColumnMetadata columnMetadata = new ColumnMetadata();
            columns.forEach(metadata -> columnMetadata.addColumn(metadata.getColumnIndex(), new Column(metadata.getColumnName())));

            List<Event> events = new ArrayList<>();
            List<EventLog> eventLogs = EventLogParser.parseEventLogFile(inputFile, columns);

            int[] eventsCounter = {0};
            int eventsPackageSize = 100;

            eventLogs.forEach(eventLog -> {
                EventRow values = new EventRow();
                int eventNum = eventLog.get("event_num");
                long timestamp = eventLog.get("timestamp");

                eventLog.getLog().forEach((metadata, value) -> {
                    LOG.debug("Event {} Column index {}", eventNum, metadata.getColumnIndex());
                    try {
                        values.addValue(metadata.getColumnIndex(), Value.createValue(EventTypeMapper.getMapping(metadata.getMainColumnType()), String.valueOf(value)));
                    } catch (IOException e) {
                        LOG.error("Value object creation failed", e);
                    }
                });

                events.add(new Event(eventNum, timestamp, values));
                eventsCounter[0]++;

                if ((eventsCounter[0] % eventsPackageSize == 0) || eventsCounter[0] == eventLogs.size()) {
                    Data data = new Data();
                    data.setHeader(header);
                    data.setColumnsMetadata(columnMetadata);
                    data.setEvents(new ArrayList<>(events));
                    events.clear();
                    dataList.add(data);
                }
            });

            LOG.info("Processed {} events as {} data batches", eventsCounter[0], dataList.size());

            return dataList;
        }
    }

    private static class BinaryDataFileProducer {

        public static int generateFile(File inputFile, File outputFile) throws IOException {
            List<Data> dataList = new ArrayList<>();
            int[] dataBlockCounter = {0};
            //fetch table columns list
            MetastoreService metastoreService = new MetastoreService();
            List<Metadata> columns = metastoreService.getTableMetadata("hcevents");

            ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
            //build header
            Header header = new Header();
            header.setProject("uhn");
            header.setSku("zeus");
            //build column metadata block
            ColumnMetadata columnMetadata = new ColumnMetadata();
            columns.forEach(metadata -> columnMetadata.addColumn(metadata.getColumnIndex(), new Column(metadata.getColumnName())));
            //build bytes section
            List<Byte> bytes = new ArrayList<>();

            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile))) {
                int[] counter = {0};
                List<Event> events = new ArrayList<>();

                bufferedReader.lines().forEach(line -> {
                    EventLog eventLog = EventLogParser.parseLine(line, columns);

                    EventRow values = new EventRow();
                    int eventNum = eventLog.get("event_num");
                    long timestamp = eventLog.get("timestamp");

                    eventLog.getLog().forEach((metadata, value) -> {
                        LOG.debug("Event {} Column index {}", eventNum, metadata.getColumnIndex());
                        try {
                            values.addValue(metadata.getColumnIndex(), Value.createValue(EventTypeMapper.getMapping(metadata.getMainColumnType()), value));
                        } catch (IOException e) {
                            LOG.error("Value object creation failed", e);
                        }
                    });

                    events.add(new Event(eventNum, timestamp, values));

                    if (++counter[0] % 100 == 0) {
                        LOG.info("Processing line {}", counter[0]);
                        convertDataToBytes(events, bytes, header, columnMetadata);
                        dataBlockCounter[0]++;
                    }
                });
                //process last part of batch
                convertDataToBytes(events, bytes, header, columnMetadata);
                dataBlockCounter[0]++;
                LOG.info("Lines processed {}", counter[0]);
                LOG.info("Generating output file {} with {} data blocks", outputFile.getAbsoluteFile(), dataBlockCounter);
                Files.write(outputFile.toPath(), Bytes.toArray(bytes));
                LOG.info("{} bytes has been written to file", bytes.size());
            } catch (Exception ex) {
                LOG.error("Exception during events log file parsing {}", inputFile.getAbsoluteFile(), ex);
            }

            return dataBlockCounter[0];
        }

        public static void generateFile(File outputFile, List<Data> dataList) throws IOException {
            MsgPackDataWriter dataWriter = new MsgPackDataWriter();
            List<Byte> bytes = new ArrayList<>();
            dataList.forEach(desData -> {
                try {
                    bytes.addAll(Bytes.asList(dataWriter.write(desData)));
                } catch (IOException e) {
                    LOG.error("Exception during data serialization {}", desData.getHeader());
                }
            });
            Files.write(outputFile.toPath(), Bytes.toArray(bytes));
            LOG.info("{} bytes has been written to file", bytes.size());
        }

        private static void convertDataToBytes(List<Event> events, List<Byte> bytes, Header header, ColumnMetadata columnMetadata) {
            MsgPackDataWriter dataWriter = new MsgPackDataWriter();

            Data data = new Data();
            data.setHeader(header);
            data.setColumnsMetadata(columnMetadata);
            data.setEvents(new ArrayList<>(events));
            events.clear();

            try {
                byte[] byteArray = dataWriter.write(data);
                short size = (short) byteArray.length;
                MD5Hash digest = MD5Hash.digest(Arrays.copyOfRange(byteArray, 0, size - 1));
                byte[] md5 = digest.getDigest();
                EventBatchHeader batchHeader = new EventBatchHeader((byte) 1, size, md5, new Date().getTime(), new byte[]{(byte) 192, (byte) 168, 0, 32});
                bytes.addAll(Bytes.asList(batchHeader.getBytes()));
                bytes.addAll(Bytes.asList(byteArray));
                LOG.info("{} have been appended to bytes container", size);
            } catch (IOException e) {
                LOG.error("Exception during data serialization {}", data.getHeader());
            }
        }
    }

    private static class BinaryDataFileConsumer {

        public static int loadFile(File inputFile, File outputFile) throws IOException {
            if (Files.exists(outputFile.toPath())) {
                Files.delete(outputFile.toPath());
            }
            Files.createFile(outputFile.toPath());
            List<Data> dataBlocks = new BinaryFileDataReader().read(inputFile.toPath());
            dataBlocks.forEach(data -> {
                try {
                    Files.write(outputFile.toPath(), data.toString().getBytes(), StandardOpenOption.APPEND);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            return dataBlocks.size();
        }
    }
}
