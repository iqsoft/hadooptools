package com.huuuge.hadoop.mr;

import com.huuuge.hadoop.deser.dto.Data;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by RWO on 6/20/2016.
 */
public class BinaryFileReadingTest {
    private static final Logger LOG = LoggerFactory.getLogger(BinaryFileReadingTest.class);
    MapDriver<IntWritable, Data, IntWritable, Data> mapDriver;
    ReduceDriver<IntWritable, Data, IntWritable, IntWritable> reduceDriver;
    MapReduceDriver<IntWritable, Data, IntWritable, Data, IntWritable, IntWritable> mapReduceDriver;

    @Before
    public void setUp() {
        BinaryFileReaderMapper mapper = new BinaryFileReaderMapper();
        BinaryFileReaderReducer reducer = new BinaryFileReaderReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    public void checkBinaryFileReading() {

    }

    private static class BinaryFileReaderMapper extends Mapper<IntWritable, Data, IntWritable, Data> {
        @Override
        protected void map(IntWritable key, Data value, Context context) throws IOException, InterruptedException {
            context.write(key, value);
        }
    }

    private static class BinaryFileReaderReducer extends Reducer<IntWritable, Data, IntWritable, IntWritable> {
        @Override
        protected void reduce(IntWritable key, Iterable<Data> values, Context context) throws IOException, InterruptedException {
            int[] amount = new int[1];
            values.forEach(data -> amount[0]++);
            context.write(key, new IntWritable(amount[0]));
        }
    }
}
