package com.huuuge.hadoop.mr;

import com.huuuge.hadoop.deser.BinaryFileDataReader;
import com.huuuge.hadoop.deser.dto.Data;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 * Created by RWO on 6/20/2016.
 */
public class CustomWritableTest {
    private static final Logger LOG = LoggerFactory.getLogger(CustomWritableTest.class);

    @Test
    public void checkWritable() throws IOException {
        String fileName = "2";
        File serFile = new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".ser");
        File tmpFile = new File("d:\\Huuuge\\Temp\\huuugeevents\\" + fileName + ".tmp");
        List<Data> dataList = new BinaryFileDataReader().read(serFile.toPath());
        dataList.forEach(dataIn -> {
            try {
                LOG.info("Serializing data {}", dataIn);
                DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(tmpFile));
                dataIn.write(outputStream);
                outputStream.flush();
                DataInputStream inputStream = new DataInputStream(new FileInputStream(tmpFile));
                Data dataOut = new Data();
                dataOut.readFields(inputStream);
                LOG.info("Deserialized data {}", dataOut);

            } catch (IOException e) {
                LOG.error("Cannot create writable data object", e);
            }
        });
    }
}
