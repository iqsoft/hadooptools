package com.huuuge.hadoop.mr.wordcount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.orc.TypeDescription;
import org.apache.orc.mapred.OrcStruct;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RWO on 6/20/2016.
 */
public class WordCountMRTest {
    MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
    ReduceDriver< Text, IntWritable, NullWritable, OrcStruct> reduceDriver;
    MapReduceDriver<LongWritable, Text, Text, IntWritable, NullWritable, OrcStruct> mapReduceDriver;

    @Before
    public void setup() {
        WordCountMapper mapper = new WordCountMapper();
        WordCountReducer reducer = new WordCountReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    public void testMapper() throws IOException {
        mapDriver.withInput(new LongWritable(1), new Text("cat cat dog"));
        mapDriver.withOutput(new Text("cat"), new IntWritable(1));
        mapDriver.withOutput(new Text("cat"), new IntWritable(1));
        mapDriver.withOutput(new Text("dog"), new IntWritable(1));
        mapDriver.runTest();
    }

    public void testReducer() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(1));
        values.add(new IntWritable(1));
        reduceDriver.withInput(new Text("cat"), values);
        OrcStruct orcStruct = new OrcStruct(TypeDescription.fromString("struct<word:string,count:int>"));
        orcStruct.setFieldValue("word", new Text("cat"));
        orcStruct.setFieldValue("count", new IntWritable(2));
        reduceDriver.withOutput(NullWritable.get(), orcStruct);
        reduceDriver.runTest();
    }
//    @Test
//    public void testMapReduce() throws IOException {
//        mapReduceDriver.withInput(new LongWritable(1), new Text("cat cat dog"));
//        mapReduceDriver.addOutput(new Text("cat"), new IntWritable(2));
//        mapReduceDriver.addOutput(new Text("dog"), new IntWritable(1));
//        mapReduceDriver.runTest();
//    }
}
