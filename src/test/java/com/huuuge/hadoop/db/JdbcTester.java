package com.huuuge.hadoop.db;

import com.huuuge.hadoop.db.dto.Metadata;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by RWO on 6/16/2016.
 */
public class JdbcTester {
    private static final Logger LOG = LoggerFactory.getLogger(JdbcTester.class);

    public void testTableMetadataFetching() throws SQLException {
        MetastoreDao dao = new MetastoreDao();
        List<Metadata> columnsMetadata = dao.getTableMetadata("hcevents");
        LOG.info("Metadata {}", columnsMetadata);
        assertTrue(columnsMetadata.size() > 0);
    }

    @Test
    public void testTableOrcSchemaFetching() throws SQLException {
        MetastoreService metastoreService = new MetastoreService();
        String orcSchema = metastoreService.getTableOrcSchema("hcevents");
        LOG.info("ORC schema {}", orcSchema);
    }
}
