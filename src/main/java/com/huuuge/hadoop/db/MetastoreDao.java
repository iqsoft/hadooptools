package com.huuuge.hadoop.db;

import com.huuuge.hadoop.db.dto.Metadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RWO on 6/16/2016.
 */
public class MetastoreDao {
    private static final Logger LOG = LoggerFactory.getLogger(MetastoreDao.class);
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";

    public List<Metadata> getTableMetadata(String tableName) {
        List<Metadata> columnsMetadata = new ArrayList<>();
        String sql = "DESCRIBE " + tableName;
        LOG.debug("Running sql: " + sql);
        try (Connection con = prepareConnection(); Statement stmt = con.createStatement()) {
            ResultSet res = stmt.executeQuery(sql);
            short columnIndex = 0;
            while (res.next()) {
                columnsMetadata.add(new Metadata(columnIndex, res.getString(1), res.getString(2)));
                columnIndex++;
            }
        } catch (SQLException sqlex) {
            LOG.error("Exception during query execution", sqlex);
        } catch (ClassNotFoundException cnfex) {
            LOG.error("Jdbc driver configuration is broken", cnfex);
        }
        return columnsMetadata;
    }

    private Connection prepareConnection() throws SQLException, ClassNotFoundException {
        Class.forName(driverName);
        return DriverManager.getConnection("jdbc:hive2://centos:10000/default", "", "");
    }
}
