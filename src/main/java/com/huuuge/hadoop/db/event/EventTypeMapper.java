package com.huuuge.hadoop.db.event;

import com.huuuge.hadoop.deser.dto.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by RWO on 6/16/2016.
 */
public class EventTypeMapper {
    private static final Map<String, Byte> mapper;

    static {
        mapper = new HashMap<>();
        mapper.put("string", Value.STRING_TYPE);
        mapper.put("byte", Value.BYTE_TYPE);
        mapper.put("short", Value.SHORT_TYPE);
        mapper.put("int", Value.INTEGER_TYPE);
        mapper.put("bigint", Value.LONG_TYPE);
        mapper.put("double", Value.DOUBLE_TYPE);
        mapper.put("map", Value.MAP_TYPE);
    }

    public static byte getMapping(String name) {
        return Optional.ofNullable(mapper.get(name)).orElse((byte) 1);
    }
}
