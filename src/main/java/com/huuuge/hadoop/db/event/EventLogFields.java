package com.huuuge.hadoop.db.event;

/**
 * Created by RWO on 6/16/2016.
 */
public interface EventLogFields {
    int SESSION_ID = 0;
    int PLAYER_ID = 1;
    int EVENT_NUM = 2;
    int TIME_STAMP = 3;
    int ARGUMENTS = 4;
}
