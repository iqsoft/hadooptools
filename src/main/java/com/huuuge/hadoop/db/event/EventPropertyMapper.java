package com.huuuge.hadoop.db.event;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by RWO on 6/16/2016.
 */
public class EventPropertyMapper {

    private static final Map<String, String> mapper;

    static {
        mapper = new HashMap<>();
        mapper.put("p", "platform");
    }

    public static String getMapping(String name) {
        return Optional.ofNullable(mapper.get(name)).orElse(name);
    }
}
