package com.huuuge.hadoop.db.event;

import com.huuuge.hadoop.db.dto.Metadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RWO on 6/16/2016.
 */
public class EventLogParser implements DataTypePrefix, EventLogFields {
    private static final Logger LOG = LoggerFactory.getLogger(EventLogParser.class);

    public static List<EventLog> parseEventLogFile(File file, List<Metadata> columns) {
        List<EventLog> events = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            int[] counter = {0};
           bufferedReader.lines().forEach(line -> {
                events.add(parseLine(line, columns));
                if (counter[0] % 1000 == 0) {
                    LOG.info("Processing line {}", counter[0]);
                }
                counter[0]++;
            });
            LOG.info("Lines processed {}", counter[0]);
        } catch (Exception ex) {
            LOG.error("Exception during events log file parsing {}", file.getAbsoluteFile(), ex);
        }
        return events;
    }

    public static EventLog parseLine(String line, List<Metadata> columns) {
        String[] parts = line.split("\\s+");

        EventLog eventLog = new EventLog(columns);
        eventLog.set("session_id", parts[SESSION_ID]);
        eventLog.set("player_id", (Long.parseLong(parts[PLAYER_ID])));
        eventLog.set("event_num", (Integer.parseInt(parts[EVENT_NUM])));
        eventLog.set("timestamp", (Long.parseLong(parts[TIME_STAMP])));

        for (int i = ARGUMENTS; i < parts.length; i++) {
            String[] keyVal = parts[i].split(":");
            //column name
            String key = keyVal[0];
            //type
            char type = keyVal[1].charAt(0);
            //value
            String val = keyVal[1].substring(1);
            //mapped column name
            String columnName = EventPropertyMapper.getMapping(key);

            switch (type) {
                case INTEGER:
                    eventLog.set(columnName, new Integer(val));
                    break;
                case LONG:
                    eventLog.set(columnName, new Long(val));
                    break;
                case STRING:
                    eventLog.set(columnName, val);
                    break;
                case BOOLEAN:
                    if (val.equalsIgnoreCase("true"))
                        eventLog.set(columnName, Boolean.TRUE);
                    else if (val.equalsIgnoreCase("false"))
                        eventLog.set(columnName, Boolean.FALSE);
                    else
                        throw new IllegalArgumentException("Failed to parse given line " + line);
                    break;
                default:
                    throw new IllegalArgumentException("Given type " + type + " of args map entry is not handled " + line);
            }
        }
        LOG.debug("Event log {}", eventLog);
        return eventLog;
    }
}
