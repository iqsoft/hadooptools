package com.huuuge.hadoop.db.event;

/**
 * Created by RWO on 6/16/2016.
 */
public interface DataTypePrefix {
    byte BOOLEAN = 'b';
    byte INTEGER = 'i';
    byte LONG = 'l';
    byte STRING = 's';
}
