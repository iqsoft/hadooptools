package com.huuuge.hadoop.db.event;

import com.huuuge.hadoop.db.dto.Metadata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by RWO on 6/16/2016.
 */
public class EventLog {

    private Map<Metadata, Object> log;
    private Map<String, Metadata> columnsMap;
    private Metadata argsMetadata;

    public EventLog(List<Metadata> columns) {
        this.log = new HashMap<>();
        this.columnsMap = new HashMap<>();
        //based on column metadata list create mapping of column name and related metadata
        columns.forEach(metadata -> this.columnsMap.put(metadata.getColumnName(), metadata));
        //search for args column in columns metadata
        argsMetadata = this.columnsMap.getOrDefault(Metadata.ARGS_NAME, Metadata.ARGS);
    }

    public void set(String columnName, Object value) {
        Metadata metadata = columnsMap.get(columnName);
        if (metadata != null) {
            log.put(metadata, value);
        } else {
            //for map type build predefined property called args as map type
            //when args is not defined in table schema we skip all properties except those defined in metastore
            Map<String, Object> args = (Map<String, Object>) log.computeIfAbsent(argsMetadata, cn -> new HashMap<String, Object>());
            args.put(columnName, value);
        }
    }

    public <T> T get(String columnName) {
        Object value;
        Metadata metadata = columnsMap.get(columnName);
        if (metadata != null) {
            value = log.get(metadata);
        } else {

            Map<String, Object> args = (Map<String, Object>) log.getOrDefault(argsMetadata, new HashMap<String, Object>());
            value = args.get(columnName);
        }
        return (T) value;
    }

    public Map<Metadata, Object> getLog() {
        return log;
    }

    @Override
    public String toString() {
        return "EventLog{" +
                "log=" + log +
                '}';
    }
}
