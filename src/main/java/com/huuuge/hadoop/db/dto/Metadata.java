package com.huuuge.hadoop.db.dto;

import java.util.Optional;

/**
 * Created by RWO on 6/16/2016.
 */
public class Metadata {
    public static final Metadata ARGS = new Metadata((short) -1, "args", "map");
    public static final String ARGS_NAME = "args";

    private final short columnIndex;
    private final String columnName;
    private final String columnType;

    public Metadata(short columnIndex, String columnName, String columnType) {
        this.columnIndex = columnIndex;
        this.columnName = columnName;
        this.columnType = columnType;
    }

    public short getColumnIndex() {
        return columnIndex;
    }

    public String getColumnName() {
        return Optional.of(columnName).get().toLowerCase();
    }

    public String getColumnType() {
        return Optional.of(columnType).get().toLowerCase();
    }

    /**
     * Normalize column types by cutting off every subtype information ie. map<string,string> become just map
     * @return
     */
    public String getMainColumnType() {
        String normalizedName = getColumnType();
        int i = columnType.indexOf('<');
        if (i > 0) {
            normalizedName = columnType.substring(0, i);
        }
        return normalizedName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Metadata metadata = (Metadata) o;

        if (columnIndex != metadata.columnIndex) return false;
        if (!columnName.equals(metadata.columnName)) return false;
        return columnType != null ? columnType.equals(metadata.columnType) : metadata.columnType == null;

    }

    @Override
    public int hashCode() {
        int result = (int) columnIndex;
        result = 31 * result + columnName.hashCode();
        result = 31 * result + (columnType != null ? columnType.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Metadata{" +
                "columnIndex=" + columnIndex +
                ", columnName='" + columnName + '\'' +
                ", columnType='" + columnType + '\'' +
                '}';
    }
}
