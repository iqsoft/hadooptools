package com.huuuge.hadoop.db;

import com.huuuge.hadoop.db.dto.Metadata;

import java.util.List;

/**
 * Created by RWO on 6/16/2016.
 */
public class MetastoreService {

    private MetastoreDao metastoreDao;

    public MetastoreService() {
        this.metastoreDao = new MetastoreDao();
    }

    public List<Metadata> getTableMetadata(String tableName) {
        return metastoreDao.getTableMetadata(tableName);
    }

    public String getTableOrcSchema(List<Metadata> tableMetadata) {
        StringBuffer buffer = new StringBuffer("struct<");
        tableMetadata.forEach((metadata) -> buffer.append(metadata.getColumnName()).append(":").append(metadata.getColumnType()).append(","));
        buffer.deleteCharAt(buffer.length()-1).append(">");
        return buffer.toString();
    }

    public String getTableOrcSchema(String tableName) {
        return getTableOrcSchema(metastoreDao.getTableMetadata(tableName));
    }
}
