package com.huuuge.hadoop;

import com.huuuge.hadoop.deser.BinaryFileDataReader;
import com.huuuge.hadoop.deser.dto.Data;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.Seekable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by RWO on 6/20/2016.
 */
public class BinaryRecordReader extends RecordReader<IntWritable, Data> {
    private static final Logger LOG = LoggerFactory.getLogger(BinaryRecordReader.class);

    private BinaryFileDataReader dataReader;

    private int splitStart;
    private int splitEnd;
    private FSDataInputStream fileIn;
    private Seekable filePosition;

    private Data data = null;
    private int dataIndex;

    @Override
    public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        LOG.info("Starting to read a split {}", split);
        Configuration conf = context.getConfiguration();
        FileSplit fileSplit = (FileSplit) split;
        splitStart = (int) fileSplit.getStart();
        splitEnd = splitStart + (int) split.getLength();
        LOG.info("Split size [{}, {}]", splitStart, splitEnd);
        Path path = fileSplit.getPath();
        final FileSystem fs = path.getFileSystem(conf);
        fileIn = fs.open(path);
        fileIn.seek(splitStart);
        filePosition = fileIn;
        dataReader = new BinaryFileDataReader();
        dataIndex = -1;
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        data = dataReader.readNext(fileIn);
        boolean hasNextValue = (data != null);
        if (hasNextValue) {
            splitStart = dataReader.getCurrentIndex();
            filePosition = fileIn;
            dataIndex++;
        }
        return hasNextValue;
    }

    @Override
    public IntWritable getCurrentKey() throws IOException, InterruptedException {
        return new IntWritable(dataIndex);
    }

    @Override
    public Data getCurrentValue() throws IOException, InterruptedException {
        return data;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        if (splitStart == splitEnd) {
            return (float)0;
        } else {
            return Math.min((float)1.0, (filePosition.getPos() - splitStart) / (float)(splitEnd - splitStart));
        }
    }

    @Override
    public void close() throws IOException {
        if (fileIn != null)
            fileIn.close();
    }
}
