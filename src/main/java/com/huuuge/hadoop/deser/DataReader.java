package com.huuuge.hadoop.deser;

import com.huuuge.hadoop.deser.dto.Data;

import java.io.IOException;

/**
 * Created by RWO on 6/15/2016.
 */
public interface DataReader {
    Data read(byte[] bytes) throws IOException;
}
