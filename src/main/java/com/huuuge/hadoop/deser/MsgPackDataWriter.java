package com.huuuge.hadoop.deser;

import com.huuuge.hadoop.deser.dto.*;
import com.huuuge.hadoop.deser.dto.Data;
import org.msgpack.MessagePack;
import org.msgpack.packer.BufferPacker;

import java.io.IOException;
import java.util.List;

/**
 * Created by RWO on 6/15/2016.
 */
public class MsgPackDataWriter implements DataWriter {

    private MessagePack msgpack;

    public MsgPackDataWriter() {
        msgpack = new MessagePack();
    }

    @Override
    public byte[] write(Data data) throws IOException {
        Header header = data.getHeader();
        ColumnMetadata columnMetadata = data.getColumnsMetadata();
        List<Event> events = data.getEvents();

        BufferPacker packer = msgpack.createBufferPacker();
        //header
        packer.write(header.getProject())
                .write(header.getSku())
                .write(header.getSessionId())
                .write(header.getPlayerId());
        //columns
        packer.writeArrayBegin(columnMetadata.getColumns().size());
        for (Column column : columnMetadata.getColumns()) {
            packer.writeArrayBegin(2)
                    .write(column.getColumnIndex())
                    .write(column.getName())
                    .writeArrayEnd();
        }
        packer.writeArrayEnd();
        //events
        packer.writeArrayBegin(events.size());
        for (Event event : events) {
            packer.writeArrayBegin(3)
                    .write(event.getNumber())
                    .write(event.getTimestamp())
                    .writeArrayBegin(event.getRow().getValues().size());
            for (Value value : event.getRow().getValues()) {
                packer.writeArrayBegin(3)
                        .write(value.getColumnIndex())
                        .write(value.getType())
                        .write(value.getValue())
                        .writeArrayEnd();
            }
            packer.writeArrayEnd()
                    .writeArrayEnd();
        }
        packer.writeArrayEnd();
        return packer.toByteArray();
    }
}
