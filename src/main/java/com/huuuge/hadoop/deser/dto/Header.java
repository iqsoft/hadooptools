package com.huuuge.hadoop.deser.dto;

import org.apache.hadoop.io.Writable;
import org.msgpack.annotation.Message;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by RWO on 6/15/2016.
 */
@Message
public class Header implements Writable, Json {


    private String project;
    private String sku;

    private String sessionId;
    private String playerId;

    public Header() {
    }

    public Header(String project, String sku, String sessionId, String playerId) {
        this.project = project;
        this.sku = sku;
        this.sessionId = sessionId;
        this.playerId = playerId;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }


    public String getProject() {
        return project;
    }

    public String getSku() {
        return sku;
    }

    public String getSessionId() {
        return Optional.ofNullable(sessionId).orElse("");
    }

    public String getPlayerId() {
        return Optional.ofNullable("".equals(playerId) ? null : playerId).orElse("-1");
    }

    @Override
    public String toString() {
        return "Header{" +
                ", project='" + getProject() + '\'' +
                ", sku='" + getSku() + '\'' +
                ", sessionId='" + getSessionId() + '\'' +
                ", playerId='" + getPlayerId() + '\'' +
                '}';
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(getProject());
        out.writeUTF(getSku());
        out.writeUTF(getSessionId());
        out.writeUTF(getPlayerId());
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        setProject(in.readUTF());
        setSku(in.readUTF());
        setSessionId(in.readUTF());
        setPlayerId(in.readUTF());
    }
}
