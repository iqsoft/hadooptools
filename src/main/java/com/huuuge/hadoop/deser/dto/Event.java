package com.huuuge.hadoop.deser.dto;

import org.apache.hadoop.io.Writable;
import org.msgpack.annotation.Message;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Created by RWO on 6/15/2016.
 */
@Message
public class Event implements Writable, Json {

    private int number;
    private long timestamp;
    private EventRow row;

    public Event() {
    }

    public Event(int number, long timestamp, EventRow row) {
        this.number = number;
        this.timestamp = timestamp;
        this.row = row;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setRow(EventRow row) {
        this.row = row;
    }

    public int getNumber() {
        return number;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public EventRow getRow() {
        return row;
    }

    @Override
    public String toString() {
        return "Event{" +
                "number=" + number +
                ", timestamp=" + timestamp +
                ", row=" + row +
                '}';
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(number);
        out.writeLong(timestamp);
        row.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        setNumber(in.readInt());
        setTimestamp(in.readLong());
        EventRow row = new EventRow();
        row.readFields(in);
        setRow(row);
    }
}
