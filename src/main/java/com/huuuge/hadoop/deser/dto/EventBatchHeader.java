package com.huuuge.hadoop.deser.dto;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by RWO on 6/27/2016.
 */
public class EventBatchHeader {
    private byte payloadFormatVersion;
    private short payloadSize;
    private byte[] payloadMD5;
    private long uploadTimestamp;
    private byte[] ip;

    public EventBatchHeader(byte payloadFormatVersion, short payloadSize, byte[] payloadMD5, long uploadTimestamp, byte[] ip) {
        this.payloadFormatVersion = payloadFormatVersion;
        this.payloadSize = payloadSize;
        this.payloadMD5 = payloadMD5;
        this.uploadTimestamp = uploadTimestamp;
        this.ip = ip;
    }

    public byte getPayloadFormatVersion() {
        return payloadFormatVersion;
    }

    public void setPayloadFormatVersion(byte payloadFormatVersion) {
        this.payloadFormatVersion = payloadFormatVersion;
    }

    public short getPayloadSize() {
        return payloadSize;
    }

    public void setPayloadSize(short payloadSize) {
        this.payloadSize = payloadSize;
    }

    public byte[] getPayloadMD5() {
        return Optional.ofNullable(payloadMD5).orElse(new byte[16]);
    }

    public void setPayloadMD5(byte[] payloadMD5) {
        this.payloadMD5 = payloadMD5;
    }

    public long getUploadTimestamp() {
        return uploadTimestamp;
    }

    public void setUploadTimestamp(long uploadTimestamp) {
        this.uploadTimestamp = uploadTimestamp;
    }

    public byte[] getIp() {
        return Optional.ofNullable(ip).orElse(new byte[4]);
    }

    public void setIp(byte[] ip) {
        this.ip = ip;
    }

    public byte[] getBytes() throws IOException {
        int size = 1 + 2 + 16 + 8 + 4;
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream(size);
        DataOutputStream outputStream = new DataOutputStream(byteOutputStream);
        outputStream.writeByte(getPayloadFormatVersion());
        outputStream.writeShort(getPayloadSize());
        outputStream.write(getPayloadMD5());
        outputStream.writeLong(getUploadTimestamp());
        outputStream.write(getIp());
        return byteOutputStream.toByteArray();
    }
}
