package com.huuuge.hadoop.deser.dto;

import org.msgpack.annotation.Message;

/**
 * Created by RWO on 6/15/2016.
 */
@Message
public class Column extends ColumnarValue {

    private String name;

    public Column() {
    }

    public Column(String name) {
        super();
        this.name = name;
    }

    public Column(short columnIndex, String name) {
        super(columnIndex);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Column{" +
                "index=" + getColumnIndex() +
                ", name='" + name + '\'' +
                '}';
    }
}
