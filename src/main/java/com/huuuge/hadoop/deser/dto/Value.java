package com.huuuge.hadoop.deser.dto;

import org.apache.hadoop.io.*;
import org.apache.orc.TypeDescription;
import org.apache.orc.mapred.OrcMap;
import org.codehaus.jackson.map.ObjectMapper;
import org.msgpack.annotation.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

/**
 * Created by RWO on 6/15/2016.
 */
@Message
public class Value extends ColumnarValue {
    private static final Logger LOG = LoggerFactory.getLogger(Value.class);

    public static final byte STRING_TYPE = 1;
    public static final byte BYTE_TYPE = 2;
    public static final byte SHORT_TYPE = 3;
    public static final byte INTEGER_TYPE = 4;
    public static final byte LONG_TYPE = 6;
    public static final byte DOUBLE_TYPE = 7;
    public static final byte MAP_TYPE = 8;

    private byte type;
    private String value;

    private static ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
    }

    public static Value createValue() {
        return new Value((short) -1, (byte) 0, null);
    }

    public static Value createValue(byte type, Object object) throws IOException {
        Value value = null;
        short columnIndex = -1;
        if (object instanceof Number) {
            value = new Value(columnIndex, type, String.valueOf(object));
        } else if (object instanceof Map) {
            value = new Value(columnIndex, type, objectMapper.writeValueAsString(object));
        } else {
            value = new Value(columnIndex, type, (String) object);
        }
        return value;
    }

    protected Value(short columnIndex, byte type, String value) {
        super(columnIndex);
        this.type = type;
        this.value = value;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setValue(Number value) {
        this.value = String.valueOf(value);
    }

    public void setValue(Map value) {
        try {
            this.value = objectMapper.writeValueAsString(value);
        } catch (IOException e) {
            LOG.error("Serialization of map {} failed", value, e);
        }
    }

    public Object getValue() {
        if (value != null) {
            switch (type) {
                case STRING_TYPE: {
                    return value;
                }
                case BYTE_TYPE: {
                    return Byte.parseByte(value);
                }
                case SHORT_TYPE: {
                    return Short.parseShort(value);
                }
                case INTEGER_TYPE: {
                    return Integer.parseInt(value);
                }
                case LONG_TYPE: {
                    return Long.parseLong(value);
                }
                case DOUBLE_TYPE: {
                    return Double.parseDouble(value);
                }
                case MAP_TYPE: {
                    return value;
                }
                default: {
                    return value;
                }
            }
        } else {
            return null;
        }
    }

    public WritableComparable getWritableValue() {
        switch (type) {
            case STRING_TYPE: {
                return new Text(String.valueOf(value));
            }
            case BYTE_TYPE: {
                return new ByteWritable(Byte.parseByte(value));
            }
            case SHORT_TYPE: {
                return new ShortWritable(Short.parseShort(value));
            }
            case INTEGER_TYPE: {
                return new IntWritable(Integer.parseInt(value));
            }
            case LONG_TYPE: {
                return new LongWritable(Long.parseLong(value));
            }
            case DOUBLE_TYPE: {
                return new DoubleWritable(Double.parseDouble(value));
            }
            case MAP_TYPE: {
                return getValueAsWritableMap();
            }
            default: {
                return new Text(String.valueOf(value));
            }
        }
    }

    public String getValueAsString() {
        return value;
    }

    private Map<String, String> getValueAsMap() {
        try {
            Map<String, String> map = objectMapper.readValue(value, Map.class);
            return map;
        } catch (IOException e) {
            LOG.error("Cannot deserialize args map from given string representation {}", value, e);
            return Collections.emptyMap();
        }
    }

    private OrcMap<Text, Text> getValueAsWritableMap() {
        TypeDescription mapSchema = TypeDescription.createMap(TypeDescription.createString(), TypeDescription.createString());
        OrcMap orcMap = new OrcMap(mapSchema);
        Map<String, String> map = getValueAsMap();
        orcMap.putAll(map);
        return orcMap;
    }

    public byte getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Value{" +
                "index=" + getColumnIndex() +
                ", type=" + type +
                ", value=" + value +
                '}';
    }

}
