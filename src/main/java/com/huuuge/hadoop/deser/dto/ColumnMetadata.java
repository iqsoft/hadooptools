package com.huuuge.hadoop.deser.dto;

import org.apache.hadoop.io.Writable;
import org.msgpack.annotation.Message;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RWO on 6/15/2016.
 */
@Message
public class ColumnMetadata implements Writable, Json {

    private List<Column> columns;

    public ColumnMetadata() {
        columns = new ArrayList<>();
    }

    public int addColumn(short index, Column column) {
        column.setColumnIndex(index);
        columns.add(index, column);
        return columns.size() - 1;
    }

    public int removeColumn(Column column) {
        column.setColumnIndex((short) -1);
        columns.remove(column);
        return column.getColumnIndex();
    }

    public int addColumn(Column value) {
        return addColumn((short) columns.size(), value);
    }

    public List<Column> getColumns() {
        return columns;
    }

    @Override
    public String toString() {
        return "ColumnMetadata{" +
                "columns=" + columns +
                '}';
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(columns.size());
        columns.forEach(column -> {
            try {
                out.writeShort(column.getColumnIndex());
                out.writeUTF(column.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        int columnsSize = in.readInt();
        for (int i=0; i<columnsSize; i++) {
            Column column = new Column(in.readShort(), in.readUTF());
            addColumn(column);
        }
    }
}
