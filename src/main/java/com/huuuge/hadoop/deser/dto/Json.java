package com.huuuge.hadoop.deser.dto;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by RWO on 6/15/2016.
 */
public interface Json extends Serializable {

    default String toJson() {
        return JSONObject.valueToString(this);
    }
}
