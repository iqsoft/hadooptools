package com.huuuge.hadoop.deser.dto;

import org.apache.hadoop.io.Writable;
import org.msgpack.annotation.Message;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.*;

/**
 * Created by RWO on 6/15/2016.
 */
@Message
public class EventRow implements Writable, Json {

    private Map<Short, Value> values;

    public EventRow() {
        values = new HashMap<>();
    }

    public int addValue(Value value) {
        values.put(value.getColumnIndex(), value);
        return values.size() - 1;
    }

    public int addValue(short index, Value value) {
        value.setColumnIndex(index);
        values.put(Short.valueOf(index), value);
        return values.size() - 1;
    }

    public int removeValue(Value value) {
        value.setColumnIndex((short) -1);
        values.remove(value);
        return value.getColumnIndex();
    }

    public Value getValue(Short index) {
        return values.get(index);
    }

    public void clearValues() {
        values.clear();
    }

    public Collection<Value> getValues() {
        return values.values();
    }

    @Override
    public String toString() {
        return "EventRow{" +
                "values=" + values +
                '}';
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(values.size());
        values.forEach((columnIndex, value) -> {
            try {
                out.writeShort(value.getColumnIndex());
                out.writeByte(value.getType());
                out.writeUTF(value.getValueAsString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        int valuesSize = in.readInt();
        for (int i=0; i<valuesSize; i++) {
            Value value = new Value(in.readShort(), in.readByte(), in.readUTF());
            addValue(value);
        }
    }
}
