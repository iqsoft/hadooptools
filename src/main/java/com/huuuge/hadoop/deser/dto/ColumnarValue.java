package com.huuuge.hadoop.deser.dto;

import org.msgpack.annotation.Message;

/**
 * Created by RWO on 6/15/2016.
 */
@Message
public abstract class ColumnarValue implements Json {

    private short columnIndex = -1;

    public ColumnarValue() {}

    public ColumnarValue(short columnIndex) {
        this.columnIndex = columnIndex;
    }

    public void setColumnIndex(short columnIndex) {
        this.columnIndex = columnIndex;
    }

    public short getColumnIndex() {
        return columnIndex;
    }
}
