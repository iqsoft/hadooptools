package com.huuuge.hadoop.deser.dto;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.Writable;
import org.msgpack.annotation.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by RWO on 6/15/2016.
 */
@Message
public class Data implements Writable, Json {
    private static final Logger LOG = LoggerFactory.getLogger(Data.class);

    private Header header;
    private ColumnMetadata columnsMetadata;
    private List<Event> events;

    public Data() {
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public ColumnMetadata getColumnsMetadata() {
        return columnsMetadata;
    }

    public void setColumnsMetadata(ColumnMetadata columnsMetadata) {
        this.columnsMetadata = columnsMetadata;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @Override
    public String toString() {
        return "Data{" +
                "header=" + header +
                ", columnsMetadata=" + columnsMetadata +
                ", events=" + events +
                '}';
    }

    @Override
    public void write(DataOutput out) throws IOException {
        //serialize header
        Header header = getHeader();
        header.write(out);
        //serialize column metadata
        ColumnMetadata columnsMetadata = getColumnsMetadata();
        columnsMetadata.write(out);
        //serialize events
        List<Writable> writableEvents = new ArrayList<>(getEvents().size());
        getEvents().forEach(event -> writableEvents.add(event));
        ArrayWritable arrayWritable = new ArrayWritable(Event.class, writableEvents.toArray(new Writable[0]));
        arrayWritable.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        //deserialize header
        Header header = new Header();
        header.readFields(in);
        //deserialize column metadata
        ColumnMetadata columnMetadata = new ColumnMetadata();
        columnMetadata.readFields(in);
        //deserialize events
        List<Event> events = new ArrayList<>();
        ArrayWritable arrayWritable = new ArrayWritable(Event.class);
        arrayWritable.readFields(in);
        Writable[] writableEvents = arrayWritable.get();
        Arrays.asList(writableEvents).forEach(writable -> events.add(((Event) writable)));

        setHeader(header);
        setColumnsMetadata(columnMetadata);
        setEvents(events);
    }
}
