package com.huuuge.hadoop.deser;

import com.huuuge.hadoop.deser.dto.*;
import com.huuuge.hadoop.deser.dto.Data;
import org.msgpack.MessagePack;
import org.msgpack.unpacker.BufferUnpacker;
import org.msgpack.util.json.JSON;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RWO on 6/15/2016.
 */
public class MsgPackDataReader implements DataReader {

    private MessagePack msgpack;

    public MsgPackDataReader() {
        msgpack = new MessagePack();
    }

    @Override
    public Data read(byte[] bytes) throws IOException {
        BufferUnpacker unpacker = msgpack.createBufferUnpacker(bytes);
        //header
        Header header = new Header(
                unpacker.readString(),
                unpacker.readString(),
                unpacker.readString(),
                unpacker.readString()
        );
        //columns
        ColumnMetadata columnMetadata = new ColumnMetadata();
        int i = unpacker.readArrayBegin();
        while(i-- > 0) {
            int j = unpacker.readArrayBegin();
            Column column = new Column(unpacker.readShort(), unpacker.readString());
            columnMetadata.addColumn(column);
            unpacker.readArrayEnd();
        }
        unpacker.readArrayEnd();
        //events
        List<Event> events = new ArrayList<>();
        i = unpacker.readArrayBegin();
        while(i-- > 0) {
            int j = unpacker.readArrayBegin();
            Event event = new Event();
            event.setNumber(unpacker.readInt());
            event.setTimestamp(unpacker.readLong());
            EventRow row = new EventRow();
            j = unpacker.readArrayBegin();
            while (j-- > 0) {
                unpacker.readArrayBegin();
                Value value = Value.createValue();
                short columnIndex = unpacker.readShort();
                value.setColumnIndex(columnIndex);
                value.setType(unpacker.readByte());
                switch (value.getType()) {
                    case 1: {
                        value.setValue(unpacker.readString());
                        break;
                    }
                    case 2: {
                        value.setValue(unpacker.readByte());
                        break;
                    }
                    case 3: {
                        value.setValue(unpacker.readShort());
                        break;
                    }
                    case 4: {
                        value.setValue(unpacker.readInt());
                        break;
                    }
                    case 6: {
                        value.setValue(unpacker.readLong());
                        break;
                    }
                    case 7: {
                        value.setValue(unpacker.readDouble());
                        break;
                    }
                    case 8: {
                        value.setValue(unpacker.readString());
                        break;
                    }
                }
                row.addValue(columnIndex, value);
                unpacker.readArrayEnd();
            }
            unpacker.readArrayEnd();
            unpacker.readArrayEnd();

            event.setRow(row);
            events.add(event);
        }
        unpacker.readArrayEnd();

        Data data = new Data();
        data.setHeader(header);
        data.setColumnsMetadata(columnMetadata);
        data.setEvents(events);

        return data;
    }
}
