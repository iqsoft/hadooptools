package com.huuuge.hadoop.deser;

import com.huuuge.hadoop.deser.dto.Data;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by RWO on 6/20/2016.
 */
public class BinaryFileDataReader {
    private static final Logger LOG = LoggerFactory.getLogger(BinaryFileDataReader.class);
    private static final int DATA_BLOCK_SIZE_BYTES = 10;

    private int currentIndex = 0;

    public Data readNext(DataInputStream dataInputStream) throws IOException {
        byte formatVersion = dataInputStream.readByte();
        short batchSize = dataInputStream.readShort();
        if (batchSize > 0) {
            LOG.info("Batch size {}", String.valueOf(batchSize));
            MsgPackDataReader dataReader = new MsgPackDataReader();
            currentIndex += 31; //event batch header size
            LOG.info("Data block size {} current index {}", batchSize, currentIndex);
            byte[] dataBytes = new byte[batchSize];
            dataInputStream.readFully(dataBytes);
            LOG.debug("Bytes after read [{}] {}", dataBytes.length, Hex.encodeHexString(dataBytes));
            Data data = dataReader.read(dataBytes);
            currentIndex += batchSize;
            LOG.info("{} data block have been deserialized", data);
            return data;
        } else {
            return null;
        }
    }

    public List<Data> read(Path inputPath) throws IOException {
        byte[] bytes = Files.readAllBytes(inputPath);
        List<Data> dataBlocks = new ArrayList<>();
        MsgPackDataReader dataReader = new MsgPackDataReader();

        ByteBuffer bb = ByteBuffer.allocate(2);
        bb.order(ByteOrder.LITTLE_ENDIAN);

        int currentIndex = 0;
        while(currentIndex < bytes.length) {
            bb.clear();
            bb.put(bytes[currentIndex+2]);
            bb.put(bytes[currentIndex+1]);
            short batchSize = bb.getShort(0);
            LOG.info("Data block size {} current index {}", batchSize, currentIndex);
            currentIndex += 31;//size of batch header
            byte[] dataBytes = Arrays.copyOfRange(bytes, currentIndex, currentIndex + batchSize);
            Data data = dataReader.read(dataBytes);
            dataBlocks.add(data);
            currentIndex += batchSize;
        }
        LOG.info("{} data blocks have been deserialized", dataBlocks.size());
        return dataBlocks;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }
}
