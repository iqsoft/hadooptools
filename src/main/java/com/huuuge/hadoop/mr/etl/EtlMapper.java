package com.huuuge.hadoop.mr.etl;

import com.huuuge.hadoop.deser.dto.Data;
import com.huuuge.hadoop.deser.dto.Event;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by RWO on 6/20/2016.
 */
public class EtlMapper extends Mapper<IntWritable, Data, IntWritable, Event> {
    private static final Logger LOG = LoggerFactory.getLogger(EtlMapper.class);

    @Override
    protected void map(IntWritable key, Data data, Context context) throws IOException, InterruptedException {
        data.getEvents().forEach(event -> {
            try {

                context.write(key, event);
            } catch (IOException | InterruptedException e) {
                LOG.error("Passing values from mapper to reducer failed", e);
            }
        });
    }
}
