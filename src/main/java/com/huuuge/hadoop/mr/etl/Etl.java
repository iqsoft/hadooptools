package com.huuuge.hadoop.mr.etl;

import com.huuuge.hadoop.BinaryFileInputFormat;
import com.huuuge.hadoop.SchemaOrcOutputFormat;
import com.huuuge.hadoop.db.MetastoreService;
import com.huuuge.hadoop.deser.dto.Data;
import com.huuuge.hadoop.deser.dto.Event;
import com.huuuge.hadoop.mr.wordcount.WordCountMapper;
import com.huuuge.hadoop.mr.wordcount.WordCountReducer;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.orc.mapreduce.OrcOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Created by RWO on 6/17/2016.
 */
public class Etl extends Configured implements Tool {
    private static final Logger LOG = LoggerFactory.getLogger(Etl.class);

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new Etl(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] args) throws Exception {
        LOG.info("ETL running with args {}", Arrays.asList(args));
        if (args.length != 2) {
            System.err.printf("Usage: %s needs two arguments, input and output files\n", getClass().getSimpleName());
            return -1;
        }

        MetastoreService metastoreService = new MetastoreService();
        String orcSchema = metastoreService.getTableOrcSchema("hcevents");

        LOG.info("OrcSchema fetched from Hive metastore {}", orcSchema);

        Job job = Job.getInstance();
        job.setJarByClass(Etl.class);
        job.setJobName("ETL");
        BinaryFileInputFormat.addInputPath(job, new Path(args[0]));

        SchemaOrcOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
        SchemaOrcOutputFormat.setOutputPath(job, new Path(args[1]));
        SchemaOrcOutputFormat.setCustomPartFileName(job, "etl");
        SchemaOrcOutputFormat.setSchema(job, orcSchema);
//        SchemaOrcOutputFormat.setSchema(job, "struct<app_id:bigint,player_id:bigint,session_id:string,timestamp:bigint,unique_event_id:bigint,event_num:int,event_type:string,st1:string,st2:string,st3:string,name:string,test:string>");
//        SchemaOrcOutputFormat.setSchema(job, "struct<data_id:string,count:int,test:string>");

        job.setInputFormatClass(BinaryFileInputFormat.class);
        job.setMapperClass(EtlMapper.class);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Event.class);
        job.setOutputFormatClass(OrcOutputFormat.class);
        job.setReducerClass(EtlReduceer.class);

        int returnValue = job.waitForCompletion(true) ? 0 : 1;
        if(job.isSuccessful()) {
            LOG.info("Job was successful");
        } else if(!job.isSuccessful()) {
            LOG.warn("Job was not successful {}", returnValue);
        }
        return returnValue;
    }
}
