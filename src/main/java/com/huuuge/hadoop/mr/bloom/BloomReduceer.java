package com.huuuge.hadoop.mr.bloom;

import com.huuuge.hadoop.deser.dto.Event;
import com.huuuge.hadoop.deser.dto.Value;
import com.huuuge.hadoop.mr.etl.EtlReduceer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.orc.TypeDescription;
import org.apache.orc.mapred.OrcStruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by RWO on 6/27/2016.
 */
public class BloomReduceer extends Reducer<IntWritable, Event, NullWritable, OrcStruct> {
    private static final Logger LOG = LoggerFactory.getLogger(EtlReduceer.class);

    @Override
    protected void reduce(IntWritable dataId, Iterable<Event> events, Context context) throws IOException, InterruptedException {
        LOG.info("Starting reducer for dataId {}", dataId);
        TypeDescription schema = TypeDescription.fromString(context.getConfiguration().get("orc.mapred.output.schema"));
        events.forEach(event -> {
            short[] columnIndex = {0};
            OrcStruct orcStruct = (OrcStruct) OrcStruct.createValue(schema);
            schema.getFieldNames().forEach(columnName -> {
                Value value = event.getRow().getValue(columnIndex[0]);
                if (value != null) {
                    LOG.info("Value {}", value);
                    WritableComparable writableValue = value.getWritableValue();
                    orcStruct.setFieldValue(columnName, writableValue);
                    LOG.info("Setting value {} for column {}", value.getValue(), columnName);

                } else {
                    orcStruct.setFieldValue(columnName, null);
                    LOG.info("Setting NULL value for column {}", columnName);
                }
                columnIndex[0]++;
            });
            try {
                LOG.info("Storing orc struct to output {}", orcStruct);
                context.write(NullWritable.get(), orcStruct);
            } catch (IOException | InterruptedException e) {
                LOG.error("ORC struct generation failed", e);
            }
        });
    }
}
