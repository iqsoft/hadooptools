package com.huuuge.hadoop.mr.bloom;

import com.huuuge.hadoop.BinaryFileInputFormat;
import com.huuuge.hadoop.SchemaOrcOutputFormat;
import com.huuuge.hadoop.db.MetastoreService;
import com.huuuge.hadoop.deser.dto.Event;
import com.huuuge.hadoop.mr.etl.Etl;
import com.huuuge.hadoop.mr.etl.EtlMapper;
import com.huuuge.hadoop.mr.etl.EtlReduceer;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.orc.mapreduce.OrcOutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Arrays;

/**
 * Created by RWO on 6/27/2016.
 */
public class Bloom extends Configured implements Tool {
    private static final Logger LOG = LoggerFactory.getLogger(Etl.class);

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new Bloom(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] args) throws Exception {
        LOG.info("Bloom running with args {}", Arrays.asList(args));
        if (args.length != 2) {
            System.err.printf("Usage: %s needs two arguments, input and output files\n", getClass().getSimpleName());
            return -1;
        }

        MetastoreService metastoreService = new MetastoreService();
        String orcSchema = metastoreService.getTableOrcSchema("hcevents");

        LOG.info("OrcSchema fetched from Hive metastore {}", orcSchema);

        String hdfsHost = "hdfs://localhost/";

        Job job = Job.getInstance();
        job.setJarByClass(Etl.class);
        job.setJobName("Bloom");
        job.getConfiguration().set("hdfs-host", hdfsHost);
        BinaryFileInputFormat.addInputPath(job, new Path(args[0]));

        SchemaOrcOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
        SchemaOrcOutputFormat.setOutputPath(job, new Path(args[1]));
        SchemaOrcOutputFormat.setCustomPartFileName(job, "bloom");
        SchemaOrcOutputFormat.setSchema(job, orcSchema);

        job.setInputFormatClass(BinaryFileInputFormat.class);
        job.setMapperClass(BloomMapper.class);
        job.setOutputFormatClass(OrcOutputFormat.class);
        job.setReducerClass(BloomReduceer.class);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Event.class);
        job.addCacheFile(new URI(hdfsHost + "/user/huuuge/data/tmp/bloom_filter#bloom_filter"));

        int returnValue = job.waitForCompletion(true) ? 0 : 1;
        if(job.isSuccessful()) {
            LOG.info("Job was successful");
        } else if(!job.isSuccessful()) {
            LOG.warn("Job was not successful {}", returnValue);
        }
        return returnValue;
    }
}