package com.huuuge.hadoop.mr.bloom;

import com.google.common.hash.Hasher;
import com.huuuge.hadoop.deser.dto.Data;
import com.huuuge.hadoop.deser.dto.Event;
import com.huuuge.hadoop.mr.etl.EtlMapper;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.client.HdfsDataInputStream;
import org.apache.hadoop.hdfs.client.HdfsDataOutputStream;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.hash.Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

/**
 * Created by RWO on 6/27/2016.
 */
public class BloomMapper extends Mapper<IntWritable, Data, IntWritable, Event> {
    private static final Logger LOG = LoggerFactory.getLogger(EtlMapper.class);

    private BloomFilter bloomFilter;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        int expectedElements = 1000*1000;
        int bitSetSize = 100*1000;
        String hdfsHost = context.getConfiguration().get("hdfs-host");

        bloomFilter = new BloomFilter(bitSetSize, expectedElements, Hash.MURMUR_HASH);
        String bloomSerialized = null;

        FileSystem fs = FileSystem.get(URI.create(hdfsHost), context.getConfiguration());
        URI bloomFileterFileURI = getBloomFilterFileURI(context);
        Path bloomFilterPath = new Path(bloomFileterFileURI);
        if (fs.exists(bloomFilterPath)) {

        }
        FSDataInputStream stream = fs.open(bloomFilterPath);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));


        if (bloomSerialized == null) {
            throw new IOException("Unable to retrieve serialized Bloom filter");
        }

        byte[] bitsetBytes = bloomSerialized.getBytes();
    }

    @Override
    protected void map(IntWritable key, Data data, Context context) throws IOException, InterruptedException {


        data.getEvents().forEach(event -> {
            try {
                context.write(key, event);
            } catch (IOException | InterruptedException e) {
                LOG.error("Passing values from mapper to reducer failed", e);
            }
        });
    }

    private URI getBloomFilterFileURI(Context context) throws IOException {
        URI bloomFileterFileURI = null;
        URI[] cacheFiles = context.getCacheFiles();
        for (URI uri: cacheFiles) {
            if (uri.getPath().endsWith("bloom_filter")) {
                bloomFileterFileURI = uri;
                break;
            }
        }
        return bloomFileterFileURI;
    }
}
