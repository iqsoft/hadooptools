package com.huuuge.hadoop.mr.wordcount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.orc.TypeDescription;
import org.apache.orc.mapred.OrcStruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by RWO on 6/14/2016.
 */
public class WordCountReducer extends Reducer<Text, IntWritable, NullWritable, OrcStruct> {
    private static final Logger LOG = LoggerFactory.getLogger(WordCountReducer.class);

    private final NullWritable nill = NullWritable.get();

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        LOG.info("Starting reducer with key {} and value {}", key.toString(), values);
        TypeDescription schema = TypeDescription.fromString(context.getConfiguration().get("orc.mapred.output.schema"));
        OrcStruct orcStruct = (OrcStruct) OrcStruct.createValue(schema);
        int sum = 0;
        for (IntWritable value : values) {
            sum += value.get();
        }
        orcStruct.setAllFields(key, new IntWritable(sum));
        LOG.info("Calculated SUM {} for key {}", sum, key);
        context.write(nill, orcStruct);
    }
}
