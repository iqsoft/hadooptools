package com.huuuge.hadoop.mr.wordcount;

import com.huuuge.hadoop.SchemaOrcOutputFormat;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by RWO on 6/14/2016.
 */
public class WordCount extends Configured implements Tool {
    private static final Logger LOG = LoggerFactory.getLogger(WordCount.class);

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new WordCount(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.printf("Usage: %s needs two arguments, input and output files\n", getClass().getSimpleName());
            return -1;
        }
        Job job = Job.getInstance();
        job.setJarByClass(WordCount.class);
        job.setJobName("WordCounter");
        FileInputFormat.addInputPath(job, new Path(args[0]));

        SchemaOrcOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
        SchemaOrcOutputFormat.setOutputPath(job, new Path(args[1]));
        SchemaOrcOutputFormat.setCustomPartFileName(job, "Test");
        SchemaOrcOutputFormat.setSchema(job, "struct<word:string,count:int>");

        job.setMapperClass(WordCountMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setReducerClass(WordCountReducer.class);
        job.setOutputFormatClass(org.apache.orc.mapreduce.OrcOutputFormat.class);
        int returnValue = job.waitForCompletion(true) ? 0 : 1;
        if(job.isSuccessful()) {
            LOG.info("Job was successful");
        } else if(!job.isSuccessful()) {
            LOG.warn("Job was not successful {}", returnValue);
        }
        return returnValue;
    }
}
