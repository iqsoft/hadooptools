package com.huuuge.hadoop.mr.wordcount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Created by RWO on 6/14/2016.
 */
public class WordCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    private static final Logger LOG = LoggerFactory.getLogger(WordCountMapper.class);

    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        LOG.info("Starting mapper with key {} and value {}", key.get(), value.toString());
        String line = value.toString();
        StringTokenizer st = new StringTokenizer(line, " ");
        while(st.hasMoreTokens()){
            word.set(st.nextToken());
            context.write(word, one);
        }
    }
}
