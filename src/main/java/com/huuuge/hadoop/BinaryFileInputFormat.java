package com.huuuge.hadoop;

import com.huuuge.hadoop.deser.dto.Data;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by RWO on 6/20/2016.
 */
public class BinaryFileInputFormat extends FileInputFormat<IntWritable, Data> {
    private static final Logger LOG = LoggerFactory.getLogger(BinaryFileInputFormat.class);
    @Override
    protected boolean isSplitable(JobContext context, Path filename) {
        LOG.info("Checking isSplitable");
        return false;
    }

    @Override
    public RecordReader<IntWritable, Data> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        return new BinaryRecordReader();
    }
}
