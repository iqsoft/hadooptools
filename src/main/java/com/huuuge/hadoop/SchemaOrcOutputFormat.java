package com.huuuge.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;
import org.apache.orc.mapreduce.OrcOutputFormat;

/**
 * Created by RWO on 6/15/2016.
 */
public class SchemaOrcOutputFormat extends org.apache.orc.mapreduce.OrcOutputFormat {

    public static void setCustomPartFileName(Job job, String name) {
        setOutputName(job, name);
    }

    public static void setSchema(Job job, String schema) {
        Configuration configuration = job.getConfiguration();
        configuration.set("orc.mapred.output.schema", schema);
    }
}
