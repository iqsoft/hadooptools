package com.huuuge.hadoop.utils;

import org.apache.commons.codec.binary.Base64;

/**
 * Created by RWO on 6/27/2016.
 */
public class Base64Utils {
    public static String fromBytes(byte[] bytes) {
        return new String(Base64.encodeBase64(bytes));
    }

    public static byte[] fromString(String string) {
        return Base64.decodeBase64(string);
    }

    public static byte[] fromBase64(byte[] base64bytes) {
        return Base64.decodeBase64(base64bytes);
    }

    public static byte[] toBase64(byte[] bytes) {
        return Base64.encodeBase64(bytes);
    }
}
